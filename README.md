Alcyon is a Perl framework for automated programs ("bots") that must interact
with websites through an API.

What is more interesting is that Alcyon is designed to do *everything* for
you: no need to know anything about network nor login mechanisms, no need to
write complicated event-driven code, no need to setup a fake MediaWiki server
for your tests; Alcyon provides a clean, simple and extensible set of objects
that deal with concurrent execution, bandwidth consumption, watching events,
performing common bot tasks, testing, and more.

What matters the most in Alcyon:

*   end-users' ease of use: everything must tend to allow writing a bot (even
    a complex one) quick and easy, because that's why Alcyon was created;

*   design: a smart and modular design is the key for a simple, extensible and
    efficient and testable software;

*   being a nice piece of code: while being compatible up to perl 5.10, Alcyon
    is implemented using recent, powerful and widely praised Perl
    technologies such as Moo, Log::Any or Dist::Zilla, and comes with a
    comprehensive documentation and testsuite.

For a more detailed introduction (how is Alcyon different from Pywikipedia?
etc.) and not-so-technical documentation, see the wiki; technical
documentation can be found, as usual, as POD in source files (and as HTML in
the tarball release, see the docs/html directory).
