package Alcyon;
# ABSTRACT: Framework to build bots interacting with any wiki through an API.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Log::Any qw($log);
use Alcyon::Exception::Standard;

=head1 SYNOPSIS

=head1 DESCRIPTION

=cut

my $DEFAULT_EXCEPTION_CLASS;
BEGIN { $DEFAULT_EXCEPTION_CLASS = 'Alcyon::Exception::Standard' }

sub throw {
    shift;
    $DEFAULT_EXCEPTION_CLASS->throw(@_);
}

sub set_default_exception_class {
    my ( $self, $desired_default ) = @_;
    $self->throw( qq{Can't use $desired_default as default exception class: }
          . qq{it doesn't implement "throw"} )
      unless $desired_default->can('throw');
    $DEFAULT_EXCEPTION_CLASS = $desired_default;
    return;
}

1;
