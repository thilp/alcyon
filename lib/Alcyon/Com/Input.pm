package Alcyon::Com::Input;
# ABSTRACT: Data Alcyon gets from its plugged Queryable resources.

use 5.010001;
use strict;
use warnings;
use utf8;

use Moo;
use MooX::Types::MooseLike::Base qw( HashRef Str );
use Scalar::Util qw( blessed );
use List::Util qw( first );

use Alcyon;

use Exporter::Easy (
    EXPORT => [ 'make_data_input', 'make_error_input' ],
);

use namespace::clean;

use overload
    '""' => \&str,
    'bool' => sub { !$_[0]->is_error };

# VERSION

=head1 SYNOPSIS

=head1 DESCRIPTION

This class describes an incoming dataset from one of the registered
L<Alcyon::Env::Queryable> resources. Instances of this class can either be
I<effective data> or I<error report>, depending on C<is_error()> being false
or true.

=for :list
* B<effective data> is what you get when everything went fine during your
  interaction with the queried resource. The resource has sent the information
  you expected. C<is_error()> is false.
* B<error report> is the opposite. The resource answered your query with an
  error status, or the communication with that resource was problematic.
  C<is_error()> is true.

Note that C<data> and C<error> attributes are read-only: so you can't alter an
Alcyon::Com::Input, you must produce a new one.

In order to easily produce I<data> or I<error> versions of Alcyon::Com::Input,
this module exports two subroutines:

=for :list
* C<make_data_input($data)>, where C<$data> is the hashref for the C<data>
  attribute;
* C<make_error_input($error)>, where C<$error> can either be a hashref or a
  simple string.

See L<EXPORT> for more details.

=cut

=attr C<data>

The content of this communication, as a (possibly multi-level) hash reference.

Note that this attribute may be set B<even if> C<is_error()> is true! In this
case, C<data> will likely contain a superset of the information displayed in
the C<error> attribute.

=cut

has data => (
    is => 'ro',
    isa => HashRef,
);

=attr C<error>

Contains the error message (as a string) if an error happened. You can easily
check that with the C<< is_error() >> predicate or the implicit boolean
conversion:

    # $input is a Alcyon::Com::Input object
    if ($input) { ... }
    # same as:
    if (!$input->is_error) { ... }

Note that the conversion is B<true> if C<is_error()> is B<false>.

=cut

has error => (
    is => 'ro',
    isa => Str,
    predicate => 'is_error',
);

=method C<str()>

Stringifies the input:

=begin :list

* if C<< $self->is_error >>, then C<< $self->error >> is returned;

* otherwise:

=for :list
* if C<< $self->data >> contains more than one key, an exception is thrown,
* otherwise, the only value (or nothing if there is no key) is returned.

=end :list

This method is called by the implicit string conversion that happens with:

    # $input is a Alcyon::Com::Input object
    say "$input";
    # same as:
    say $input->str;

=cut

sub str {
    my $self = shift;
    if ($self->is_error) {
        return $self->error;
    }
    else {
        my $data = $self->data;
        if (!%$data) { return '' }
        elsif ( keys %$data > 1 ) {
            Alcyon->throw( q{can't stringify an Alcyon::Com::Input }
                  . q{which "data" is a more-than-one-key hash} );
        }
        else {
            my (undef, $value) = each %$data;
            return $value;
        }
    }
}

=head1 SUBROUTINES

These subroutines are I<automatically exported> so that other classes can
easily create valid Alcyon::Com::Input instances, i.e. without the cumbersome
C<new()>.

They are the recommended way to build Alcyon::Com::Input instances.

=head2 C<make_data_input( $data )>

Returns an Alcyon::Com::Input C<$d> such as:

=for :list
* C<< $d->is_error >> is false
* C<< $d->data == $data >>

=cut

sub make_data_input {
    my $data = shift;
    $data = shift if @_ && blessed($data); # don't care about the $class
    return __PACKAGE__->new( data => $data );
}

=head2 C<make_data_input( $error )>

=head2 C<< make_data_input( $error, errormsg => $str ) >>

Returns an Alcyon::Com::Input C<$e> such as:

=begin :list

* C<< $e->is_error >> is true

* B<if> C<$error> is a B<string>: C<< $e->error == $error >>

* B<if> C<$error> is a B<hashref>: C<< $e->data == $error >>. Moreover, if
  C<$str> is given, C<< $e->error >> is the value of C<< $error->{$str} >>
  (an exception is thrown if that's not a string); otherwise, C<< $e->error >>
  is the value of one of C<$error>'s keys, chosen in the following order:

=for :list
1. If C<< $error->{error} >> exists and its value is a string, that string is used.
1. If C<< $error->{message} >> exists and its value is a string, that string is used.
1. If C<< $error->{msg} >> exists and its value is a string, that string is used.
1. If C<< $error->{code} >> exists and its value is a string, that string is used.
1. If one of the top-level keys' values is a string, that string is used.
1. The string C<< "(no message found)" >> is used.

=end :list

=cut

sub make_error_input {
    my $error = shift;
    $error = shift if blessed($error);
    if ( ref $error eq 'HASH' ) {
        my %opts;
      SEARCH:
        unless ( %opts = @_ ) {
            ($opts{errormsg}) = map { $error->{$_} } first {
                exists $error->{$_}
                  && do { my $a = $error->{$_}; defined $a && !ref $a }
            }
            qw( error message msg code );
            unless ( defined $opts{errormsg} ) {
                while ( my ( undef, $v ) = each %$error ) {
                    $opts{errormsg} = $v and last SEARCH
                      if defined $v && !ref $v;
                }
                $opts{errormsg} = '(no message found)';
            }
        }
        return __PACKAGE__->new( data => $error, error => $opts{errormsg} );
    }
    else {
        return __PACKAGE__->new( error => $error );
    }
}

1;
