package Alcyon::Env::Queryable;
# ABSTRACT: Role for all ressources that can be queried, and therefore deal with watchdogs.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Carp 'confess';
use Scalar::Util 'blessed';
use Log::Any qw($log);
use Alcyon;
use Moo::Role;
use namespace::clean;

=head1 DESCRIPTION

This role must be done by all ressources (e.g. databases, APIs, etc.) which:

=for :list
* are accessed through L<Alcyon::Env>;
* expect an L<Alcyon::Com::Query> to work with.

=method C<< request( $query ) >>

B<Required.>
Performs C<$query> (a L<Alcyon::Com::Query> object) and returns an
L<Alcyon::Com::Input>.

This role modifies C<request()> in the following ways:

=for :list
* type-checking is performed on the first argument:
    C<< blessed($_[0]) and $_[0]->isa('Alcyon::Com::Query') >>
* registered watchdogs are kept informed of the query and its result.

=cut

requires 'request';

around request => sub {
    my $method   = shift;

    # Tell mistress
    $log->debugf('Queryable::request( %s ) called', $_[0]);

    # Type checking
    my $expected = 'Alcyon::Com::Query';
    confess "request() got a non-$expected argument"
      unless blessed($_[0]) and $_[0]->isa($expected);

    # Call the specialized request()
    my $data = $method->(@_);

    # Inform watchdogs about this query and its result
    Alcyon->watchdogs->inform(\$_[0], \$data);

    return $data;
};

1;
