package Alcyon::Types;
# ABSTRACT: Some types defined for use in Alcyon.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Scalar::Util qw( blessed );
use List::MoreUtils qw( any );

use MooX::Types::MooseLike;
use base qw(Exporter);
our @EXPORT_OK = ();

=head1 DESCRIPTION

All these types can be used as barewords (they actually are subroutines) after
import. For instance:

    use Alcyon::Types qw( LogLevel );

=head2 LogLevel

Enumeration grouping log levels offered by L<Log::Any>:

=for :list
* C<"trace">
* C<"debug">
* C<"info">
* C<"notice">
* C<"warning">
* C<"error">
* C<"critical">
* C<"alert">
* C<"emergency">

=cut

my $defs = [
    {
        name => 'LogLevel',
        test => sub {
            defined $_[0] && !blessed( $_[0] ) && any { $_[0] eq $_ }
            qw< trace debug info notice warning
              error critical alert emergency >;
        },
        message => sub { "$_[0] is not one of the levels defined in Log::Any!" }
    },
];

# Make the types available - this adds them to @EXPORT_OK automatically.
MooX::Types::MooseLike::register_types( $defs, __PACKAGE__ );

1;
