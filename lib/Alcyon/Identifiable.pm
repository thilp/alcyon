package Alcyon::Identifiable;
# ABSTRACT: Role for objects that store an uid.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo::Role;
use MooX::Types::MooseLike::Base qw(Int);
use Sub::Quote;
use namespace::clean;

=attr C<uid>

Read-only. The first I<Identifiable> object is assigned UID 0, and each
subsequent I<Identifiable> is created with an incremented UID.

=cut

my $BASE_ID = 0;

has uid => (
    is => 'ro',
    isa => 'Int',
    default => quote_sub(q{ $BASE_ID++ }),
);

1;
