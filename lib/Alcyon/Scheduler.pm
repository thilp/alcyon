package Alcyon::Scheduler;
# ABSTRACT: Run all registered Alcyon modules.

use 5.010001;
use strict;
use warnings;

# VERSION

use Moo;
use MooX::Types::MooseLike;
use Log::Any qw($log);
use namespace::clean;

=head1 SYNOPSIS

    # Some modules we want to run
    my $m1 = Alcyon::Module::Mod1->new(...);
    my $m2 = Alcyon::Module::Mod2->new(...);
    my $m3 = Alcyon::Module::Mod3->new(...);
    my $m4 = Alcyon::Module::Mod4->new(...);

    # Get the main scheduler (throws an exception if called in Alcyon::Module)
    my $sched = Alcyon->scheduler;

    # Now, schedule module execution:

    # loop ...
    $sched->loop(
        [ $m1, $m2 ],    # run both $m1 and $m2 concurrently ...
        $m3,             # ... then $m3 alone ...
        $m4,             # ... then $m4 alone ...
    );

    # ... or one-shot
    $sched->once( [ $m1, $m2 ], $m3, $m4 );

    # Run registered modules with the last schedule (in a distinct thread).
    # Will throw an Alcyon::
    $sched->run;

    if ( $sched->is_running ) {
        # Alcyon::Module::name gets the module's name
        say "Executing module ", $sched->last_run->name;
    }
    elsif ( my $exception = $sched->error ) {    # Alcyon::Scheduler::Exception
        say "An error occurred: $exception";     # use overload '""'
    }
    else {
        say "Already done! The last module run was ", $sched->last_run->name;
    }

    # Interrupt or abort the schedule anytime (won't set $sched->error)
    $sched->pause;     # freeze module execution ...
    $sched->continue;  # ... until now
    $sched->finish;    # stop (won't continue()) after the current schedule step
    $sched->abort;     # stop right now (even in the middle of a module!)

=head1 DESCRIPTION

An Alcyon::Scheduler instance runs each Alcyon::Module it has previously
registered, until it is stopped or there is nothing more to run (depending on
the chosen schedule).

=cut

has _tasks => (
    is => 'rwp',
    isa => ''
);

=method once

=method loop

=method run

=method pause

=method continue

=method finish

=method abort

=method is_running

=method last_run

=method error

=cut

1;
