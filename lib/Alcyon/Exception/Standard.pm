package Alcyon::Exception::Standard;
# ABSTRACT: An Alcyon::Exception class that logs, traces and categorizes.

use 5.010001;
use strict;
use warnings;
use utf8;

use Moo;
use namespace::clean;

extends 'Alcyon::Exception';
with 'Alcyon::Exception::Loggable';
with 'Alcyon::Exception::Categorizable';
with 'StackTrace::Auto';

# VERSION

=head1 SYNOPSIS

    Alcyon::Exception::Standard->throw(
        msg      => 'Everything is broken!',
        category => 'PHP',
        loglevel => 'critical'
    );

    # Or, in Alcyon:
    Alcyon->throw(
        msg      => 'Everything is broken!',
        category => 'PHP',
        loglevel => 'critical'
    );

=head1 DESCRIPTION

This class does nothing more than extending L<Alcyon::Exception> by consuming
the following roles:

=for :list
* L<StackTrace::Auto> to register and display a stacktrace,
* L<Alcyon::Exception::Loggable> to log the exception in the logging system,
* L<Alcyon::Exception::Categorizable> to allow simple categorization.

Note that the main L<Alcyon> class provides a shortcut for throwing instances
of this class (see L<SYNOPSIS>). See the documentation of C<throw> in
L<Alcyon> to learn how to use another class with this shortcut.

=cut

1;
