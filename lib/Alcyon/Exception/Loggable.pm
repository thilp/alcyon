package Alcyon::Exception::Loggable;

# ABSTRACT:

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo::Role;
use MooX::Types::MooseLike::Base qw( Int );
use Log::Any qw($log);
use Sub::Quote;
use Alcyon::Types qw( LogLevel );

=head1 SYNOPSIS

=head1 DESCRIPTION

This role creates an entry in the logging system (L<Log::Any>) when the
exception is built. The text comes from C<Alcyon::Exception::msg>, and thus
always exists.

If your consuming class also I<does> L<Alcyon::Exception::Categorizable> and a
category is set, that category is prepended to the message.

B<Beware:> the C<new> is logged, not the C<throw> (which calls C<new>), so the
logging system will be called even if you don't C<throw> your exception.

=cut

requires 'msg'; # for the log message

=attr C<loglevel>

An L<Alcyon::Type::LogLevel> that specifies the level used when creating an
entry in the logging system. Optional, defaults to C<"warning">.

=cut

has loglevel => (
    is => 'ro',
    isa => LogLevel,
    default => 'warning',
);

# In case the consuming class doesn't provide one
sub BUILD { }

after BUILD => sub {
    my $self = shift;

    my $message = $self->msg;

    # If the class does Alcyon::Exception::Categorizable, we can use category
    if (   $self->does('Alcyon::Exception::Categorizable')
        && $self->has_category )
    {
        $message = '[' . $self->category . '] ' . $message;
    }

    # Log::Any doesn't provide a generic "log this" method expecting the
    # log level as argument, but since we enforced what values loglevel can
    # take (with Alcyon::Types::LogLevel), we can safely use it as a
    # subroutine name!
    my $level = $self->loglevel;
    $log->$level('Exception created: ' . $message);
};

1;
