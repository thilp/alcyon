package Alcyon::Exception::Categorizable;
# ABSTRACT: Role for exceptions that have a category.

use 5.010001;
use strict;
use warnings;
use utf8;

use Moo::Role;
use MooX::Types::MooseLike::Base qw( Str );

# VERSION

=head1 DESCRIPTION

This role adds an optional C<category> attribute to store a (usually short)
string describing the context of this exception, with a C<has_category>
predicate to know if a C<category> has been provided.

=cut

has category => (
    is => 'ro',
    isa => Str,
    predicate => 1,
);

1;
