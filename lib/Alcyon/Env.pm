package Alcyon::Env;

# ABSTRACT: The whole environment of an Alcyon instance.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo;
use MooX::Types::MooseLike::Base qw( HashRef );
use Carp 'confess';
use Scalar::Util 'blessed';
use Log::Any qw($log);
use namespace::clean;

=head1 SYNOPSIS

=head1 DESCRIPTION

All communications with databases, APIs, the clock or the filesystem
happen through the Alcyon instance's Env object. This allow to easily branch
any new component and to simulate some components with mockup objects during
tests.

=head2 Design

This design is inspired from
L<Moonpig|http://blog.plover.com/prog/Moonpig.html> by MJD and RJBS.  However,
in Moonpig, each resource is unique (I<one> database, I<one> filesystem, etc.)
whereas Alcyon-based bots may need more than one instance of a resource (e.g.
multiple APIs for interwiki links management). Besides, attributing one method
name per resource might not be very easy to extend.  For these purposes,
instead of argumentless methods like C<api>, C<db>, etc., Alcyon uses C<on()>,
that expects a string – the name under which a corresponding resource has been
registered.

=head2 Resource registration

The C<on('RESOURCE')> call will die unless the corresponding resource has been
registered (which, here, means I<associated with the string> C<"RESOURCE">,
which is its I<identifier>).

To register components (in a subclass of Alcyon::Env, because Alcyon::Env
itself does not register any component), just use the C<register()>
method in C<BUILD>:

    package MyEnv;
    use Moo;
    use namespace::clean;
    extends 'Alcyon::Env';

    sub BUILD {
        $_[0]->register( api   => $my_api );
        $_[0]->register( timer => $my_timer );
    }

This will populate the hashtable that Alcyon::Env checks when C<on()> is
called:

    # later, while using the MyEnv environment ...

    Alcyon->env->on('timer')->some_method;     # alive!
    Alcyon->env->api->some_method;             # still alive!
    Alcyon->env->on('totoro')->some_method;    # dies, as well as ->totoro()

You shouldn't try to register (or even unregister) resources after C<BUILD>,
as the current implementation could evolve and enforce immutability.

=head2 Shortcuts

Alcyon has been designed with the use of multiple instances of the same
resource type in mind, but if your application extensively uses only one
resource of some type, writing C<on('resource')> each time might get tedious.

To avoid such tediousness, Alcyon::Env uses C<AUTOLOAD> to provide you with
shortcuts for C<on()>. Thus, instead of:

    Alcyon->env->on('RESOURCE')->do_something

you may write:

    Alcyon->env->RESOURCE->do_something

As long as C<RESOURCE> is not formally defined by Alcyon::Env, this will do
The Right Thing. B<But be wary> of Alcyon::Env possible changes, and choose
your resource's name with care! Here are some names that you are guaranteed
will I<never> be used for anything else in Alcyon::Env (they actually I<are>
formally defined as shortcuts):

=for :list
* C<api>, a shorthand for C<on('api')>
* C<db>, a shorthand for C<on('db')>
* C<fs>, a shorthand for C<on('fs')>
* C<time>, a shorthand for C<on('time')>
* C<web>, a shorthand for C<on('web')>

Note that the use of C<AUTOLOAD> implies a (slight) performance cost, since the
call is logged (with priority "debug") and the method's name is cleaned.
Nothing such happens with C<on()> nor with the 5 aforementioned shortcuts, as
they do not use C<AUTOLOAD>.

=cut

sub api  { unshift @_, 'api';  goto &on }
sub db   { unshift @_, 'db';   goto &on }
sub fs   { unshift @_, 'fs';   goto &on }
sub time { unshift @_, 'time'; goto &on }
sub web  { unshift @_, 'web';  goto &on }

sub AUTOLOAD {
    my ($sub) = our $AUTOLOAD =~ m{ (?<=::) ( [^:]+ ) \z }x;
    $log->debug( qq{In Alcyon::Env: $AUTOLOAD }
          . qq{(used instead of ->on('$sub')) }
          . qq{simulated via AUTOLOAD} );
    unshift @_, $sub;
    goto &on;
}

=method C<< on( $str_id ) >>

This method returns the resource identified by C<$str_id>, which is the string
associated to the resource during a C<register()> call (see
L<Resource registration>).
The resource returned I<does> the L<Alcyon::DataSource> role.

It dies with a backtrace if the appropriate resource can't be located (which
means C<$str_id> was probably not C<register>ed).

=cut

# This is where ($str_id, $resource) couples are stored. Therefore, $_map is
# the structure that register() fills and that on() queries.

has _MAP => (
    is      => 'ro',
    isa     => HashRef['Alcyon::DataSource'],
    default => sub { return {} },
);

# This function is called a lot, so it should be as fast as possible.

sub on {
    # A little type-safety does not hurt in regard to what it brings ...
    confess "The (first) argument of Alcyon::Env::on() must be a string"
      unless defined( $_[1] ) && !ref( $_[1] );

    $_[0]->_MAP->{ $_[1] }
      || confess qq{Tried to grab unknown resource "$_[1]"};
}

=method C<< register( $str_id, $resource ) >>

Associates the string C<$str_id> to the L<Alcyon::DataSource> object
C<$resource> and enable further usage I<via> C<on($str_id)>. See L<Resource
registration> for more information.

This method dies in the following cases:

=for :list
* it is called with invalid arguments (types or quantity);
* a resource has already been C<register>ed (in a parent class) with C<$str_id>.

Otherwise it returns 1.

=cut

sub register {
    my $self = shift;

    # We can do all the type-checking we want since this function is only
    # called a few times at initialization.

    my $str = shift
      or confess "Alcyon::Env::register() expects 2 arguments, 0 given";
    confess "Alcyon::Env::register() expects a string as first argument"
      unless !ref $str;

    my $resource = shift
      or confess "Alcyon::Env::register() expects 2 arguments, 1 given";
    my $expected = 'Alcyon::DataSource';
    confess "Alcyon::Env::register() expects a $expected as second argument"
      unless blessed($resource) && $resource->does($expected);

    # Now on type registration

    # Die if the same $str has already been defined in a parent class
    confess qq{Alcyon::Env::register: a resource identified by "$str" }
      . qq{has already been registered in a parent class}
      if $self->_MAP->{$str};

    $self->_MAP->{$str} = $resource;

    # Tell mistress
    $log->info(qq{Alcyon::Env: "$str" resource registered});

    return 1;
}

=method C<< resources() >>

Returns a list of registered resource identifiers (i.e. the strings used in
each C<register()> call).

To put it another way: Strings returned by this method are I<exactly> the
arguments C<on()> will accept.

=cut

sub resources { keys %{ $_[0]->_MAP } }

1;
