package Alcyon::Exception;
# ABSTRACT: Exceptions in Alcyon

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo;
use MooX::Types::MooseLike::Base qw( Str );
use namespace::clean;

with 'Throwable';

=head1 SYNOPSIS

    Alcyon::Exception->throw( 'message' );

=head1 DESCRIPTION

Alcyon::Exception is the central part of Alcyon's error system, which provides
stacktraces, interaction with the logging system, etc. using roles in the
Alcyon::Exception::* namespace.

In its simplest form (i.e. without any role applied), an Alcyon::Exception
just C<die>s with a message. See L<Alcyon::Exception::Standard> for a more
useful exception class.

=attr C<msg>

The message given when the exception was thrown, or C<"(no message)"> if
nothing was given.

=cut

has msg => (
    is => 'ro',
    isa => Str,
    default => '(no message)',
);

# To allow the ->throw( 'message' ) call, even though msg is required.
sub BUILDARGS {
    shift;    # evacuate the $class
    unshift @_, 'msg' if @_ % 2;    # odd number of arguments, prepend 'msg'
    return {@_};
}

1;
