package Alcyon::Scheduler::Job::SingleJob;
# ABSTRACT: A job for just one module.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo;
use MooX::Types::MooseLike::Base qw(Int);
use Log::Any qw($log);
use namespace::clean;

with 'Alcyon::Scheduler::Job';

=head1 DESCRIPTION

This kind of job contains only one module, and thus its C<run> method 

=cut

1;
