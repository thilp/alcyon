package Alcyon::Scheduler::Job;
# ABSTRACT: Role that represents a scheduler step.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo::Role;
use namespace::clean;

with 'Alcyon::Identifiable';

=head1 DESCRIPTION

The L<Alcyon::Scheduler> does not want to know anything about how to execute
parallel or cooperative jobs nor anything else: its only purpose is to B<run>
jobs, one after the other, and stop when appropriate. So, we encapsulate the
job-execution logic in the Scheduler::Job::* classes, and Scheduler interacts
with these classes through the interface the Scheduler::Job role provides.

=method C<run()>

Executes modules the consumer class represents. Returns 0 in case of errors, 1
otherwise.

B<Required> by this role.

=cut

requires 'run';

=attr C<uid>

See L<Alcyon::Identifiable>.

=cut

1;
