package Alcyon::Scheduler::Schedule;
# ABSTRACT: Stores registered Alcyon::Module and their planned execution
# order, and iteratively produces Alcyon::Scheduler::Job instances to
# represent them.

use 5.010001;
use strict;
use warnings;
use utf8;

# VERSION

use Moo;
use MooX::Types::MooseLike::Base;
use Log::Any qw($log);
use namespace::clean;

=head1 SYNOPSIS

    my $schedule = Alcyon::Scheduler::Schedule->new(
        loop => 1,  # or 0 for one-shot execution
        plan => [ $m1, [ $m21, $m22 ] ],
    );

    my $first_job  = $schedule->next;    # Alcyon::Scheduler::Job for $m1
    my $second_job = $schedule->next;    # idem for $m21 + $m22 concurrently
    my $third_job  = $schedule->next;    # idem for $m1

=head1 DESCRIPTION

This class stores Alcyon modules and distributes them back as
Alcyon::Scheduler::Job instances.

=cut

1;
