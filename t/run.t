#!/usr/bin/env perl -T

use Test::Class::Moose::Load 't';

Test::Class::Moose->new(
    show_timing => 0,
    randomize   => 0,
    statistics  => 1,
    test_classes => \@ARGV,
)->runtests;
