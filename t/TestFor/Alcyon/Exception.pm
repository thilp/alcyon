package TestFor::Alcyon::Exception;
use Test::Class::Moose extends => 'TestFor';
use Test::Fatal;
with 'Test::Class::Moose::Role::AutoUse';

# For subclassing
sub extra_constructor_args { }

my $message = 'Totoro';

sub test_throwable {
    my $test = shift;
    my $e = exception { $test->class_name->throw( extra_constructor_args() ) };

    ok( defined $e, 'An exception is thrown with ->throw()' );

    isa_ok( $e, $test->class_name );
}

sub test_msg {
    my $test = shift;

    {    # Empty message
        my $e = $test->class_name->new();
        is( $e->msg, '(no message)',
            '"(no message)" is used when no msg argument is provided' );
    }

    {    # Unnamed argument
        my $e = $test->class_name->new($message);
        is( $e->msg, $message, 'BUILDARGS does its job' );
    }

    {    # Named argument
        my $e = $test->class_name->new( msg => $message );
        is( $e->msg, $message, 'standard usage of the constructor' );
    }
}

1;
