package TestFor::Alcyon::Exception::Standard;
use Test::Class::Moose extends => 'TestFor::Alcyon::Exception';
use Test::Fatal;
use Log::Any::Test;
use Log::Any qw($log);

my $message = 'BlaBla';

sub extra_constructor_args {
    my $test = shift;
    return (
        msg => $message,
        $test->SUPER::extra_constructor_args()
    );
}

sub test_category {
    my $test = shift;

    can_ok( $test->class_name, 'category' );

    {
        my $e = exception {
            $test->class_name->throw( $test->extra_constructor_args() );
        };
        use Data::Dumper;
        ok( !$e->has_category, 'has_category false when no category provided' );
    }

    {
        my $e = exception {
            $test->class_name->throw(
                category => 'network',
                $test->extra_constructor_args()
            );
        };
        ok( $e->has_category, 'has_category true when category provided' );
        is( $e->category, 'network', 'categories are remembered' );
    }
}

sub test_logging {
    my $test = shift;
    $log->clear; # remove the noise from the other tests

    {    # Log with default level "warning"
        my $e = exception {
            $test->class_name->throw( $test->extra_constructor_args() );
        };
        fail('exception expected, but nothing was thrown') unless $e;
        fail( $test->class_name . ' expected, but got ' . ref($e) )
          if ref($e) ne $test->class_name;
        my $logs = $log->msgs;
        is( scalar @{$logs}, 1, 'only one entry was added in the log' );
        is( $logs->[0]->{level},
            'warning', 'exception thrown with default loglevel' );
        like( $logs->[0]->{message},
            qr{\b\Q$message\E\b}, 'log contains the expected message' );
        $log->clear;
    }

    {    # Log with level "critical"
        my $e = exception {
            $test->class_name->throw(
                loglevel => 'critical',
                $test->extra_constructor_args()
            );
        };
        fail('exception expected, but nothing was thrown') unless $e;
        fail( $test->class_name . ' expected, but got ' . ref($e) )
          if ref($e) ne $test->class_name;
        my $logs = $log->msgs;
        is( scalar @{$logs}, 1, 'only one entry was added in the log' );
        is( $logs->[0]->{level},
            'critical', 'exception thrown with expected level' );
        $log->clear;
    }

    {    # Log with level "info" + prepend the category
        my $e = exception {
            $test->class_name->throw(
                loglevel => 'info',
                category => 'network',
                $test->extra_constructor_args()
            );
        };
        fail('exception expected, but nothing was thrown') unless $e;
        fail( $test->class_name . ' expected, but got ' . ref($e) )
          if ref($e) ne $test->class_name;
        my $logs = $log->msgs;
        is( scalar @{$logs}, 1, 'only one entry was added in the log' );
        is( $logs->[0]->{level},
            'info', 'exception thrown with expected level' );
        like(
            $logs->[0]->{message},
            qr/\[network\] \Q$message\E/,
            'log entry contains the specified category'
        );
        $log->clear;
    }
}

1;
