package TestFor::Alcyon::Com::Input;
use Test::Modern -requires => {
    'Package::Stash' => undef,
};
use Test::Class::Moose extends => 'TestFor';
with 'Test::Class::Moose::Role::AutoUse';

# For subclassing
sub extra_constructor_args { }

has [qw[ test_methodlist test_exportlist ]] => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
);

sub test_startup {
    my $test = shift;
    $test->test_exportlist( [qw[ make_data_input make_error_input ]] );
    $test->test_methodlist(
        [
            qw[ new data error is_error str (bool (( ("" ],
            @{ $test->test_exportlist },
        ]
    );
}

sub test_api {
    my $test = shift;
    import_ok( $test->class_name, export => $test->test_exportlist );
    class_api_ok( $test->class_name, @{ $test->test_methodlist } );
}

sub test_content {
    my $test = shift;

    my $content = 'BlaBlaBla';

    my $class    = $test->class_name;
    my $sub_make = 'make_data_input';
    no strict 'refs';
    Package::Stash->new(__PACKAGE__)
      ->add_symbol( '&make_input', \&{"${class}::$sub_make"} );

    {
        my $data      = { msg => $content };
        my $with_make = make_input($data);
        my $with_new  = $test->class_name->new(
            data => $data,
            extra_constructor_args(),
        );

        for my $h ( [ $with_new, '$new' ], [ $with_make, '$make_data_input' ] ) {
            object_ok(
                $h->[0],
                $h->[1],
                isa   => $test->class_name,
                can   => $test->test_methodlist,
                #clean => 1,
                more  => sub {
                    my $o = shift;
                    cmp_deeply( $o->data, $data,
                        '"Data" input has expected content' );
                    ok( !$o->is_error, '"Data" input has no error' );
                    is( $o->str, $content,
                        'string conversion works with a one-key hash');
                    is( "$o", $content,
                        'automatic string conversion works with a one-key hash');
                },
            );
        }
    }

    # Now check that string conversion throws an exception with a
    # more-than-one-key hash:

    {
        my $data      = { msg => $content, other => 'key' };
        my $with_make = make_input($data);
        my $with_new  = $test->class_name->new(
            data => $data,
            extra_constructor_args(),
        );

        for my $h ( [ $with_new, '$new' ], [ $with_make, '$make_data_input' ] ) {
            my $e = exception { "$h->[0]" };
            ok( $e,
                    'string conversion throws an exception '
                  . "with a more-than-one-key hash ($h->[1])" );
            can_ok( $e, 'msg' );
            like( $e->msg, qr/\bmore-than-one-key hash\b/,
                'exception message is explicit'
            );
        }
    }
}

sub test_error {
    my $test = shift;

    my $content = 'BlaBlaBla';

    my $sub_make = 'make_error_input';
    {
        no strict 'refs';
        Package::Stash->new(__PACKAGE__)
          ->add_symbol( '&make_input',
            \&{ $test->class_name . '::' . $sub_make } );
    }

    my $data      = { msg => $content };
    my $error     = $content;
    my $with_make = make_input($data);
    my $with_new  = $test->class_name->new(
        data  => $data,
        error => $error,
        extra_constructor_args(),
    );

    for my $h ( [ $with_new, 'new' ], [ $with_make, 'make_error_input' ] ) {
        object_ok(
            $h->[0],
            $h->[1],
            isa   => $test->class_name,
            can   => $test->test_methodlist,
            #clean => 1,
            more  => sub {
                my $o = shift;
                cmp_deeply( $o->data, $data,
                    '"Error" input has expected "data" content' );
                is( $o->error, $error,
                    '"Error" input has expected "error" content' );
                ok( $o->is_error, '"Error" input "is_error"' );
                is( $o->str, $error,
                    'string conversion works with "error" input' );
                is( "$o", $error,
                    'automatic string conversion works with "error" input' );
                if ($o) {
                    fail 'automatic boolean conversion of "error" input failed';
                }
                else {
                    pass 'automatic boolean conversion of "error" input works';
                }
            },
        );
    }
}

1;
