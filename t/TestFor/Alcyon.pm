package TestFor::Alcyon;
use Test::Class::Moose extends => 'TestFor';
use Test::Fatal;
with 'Test::Class::Moose::Role::AutoUse';

sub test_default_exception_class {
    my $test        = shift;
    my $default     = 'Alcyon::Exception::Standard';
    my $new_default = 'Alcyon::Exception';

    can_ok( $test->class_name, 'throw' );

    isa_ok( exception { $test->class_name->throw() }, $default );

    isa_ok(
        exception {
            $test->class_name->set_default_exception_class(
                'TestFor::UnexistentClass');
        },
        $default
    );

    isa_ok(
        exception {
            $test->class_name->set_default_exception_class('TestFor')
              ;    # because TestFor can()'t "throw"
        },
        $default
    );

    is(
        exception {
            $test->class_name->set_default_exception_class($new_default)
              ;    # Alcyon::Exception can() "throw"
        },
        undef,
        'allow to change the default exception class by one that can("throw")'
    );

    isa_ok( exception { $test->class_name->throw() }, $new_default );
}

1;
